/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-05
 */
package inf1256s05;
import java.util.*;
public class BoucleInfinieEtBreak {

	public static void main(String[] args) {
		
         Scanner clavier = new Scanner(System.in);
         while(true){
        	 System.out.println("Entrez Un nombre entier pour arreter cette boucle infinie");
        	 if(clavier.hasNextInt()){
        		 break;
        	 }
        	 clavier.next();
         }
         int nbStop =clavier.nextInt();
         System.out.format("Voici le nombre qui a arreté la boucle: %d %n",nbStop);
         clavier.close();
	}

}
