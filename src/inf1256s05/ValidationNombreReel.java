/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-05
 */
package inf1256s05;
import java.util.*;
public class ValidationNombreReel {

	public static void main(String[] args) {
		// Validation nombre 2 chiffres avant point et 1 à 3 chiffres après
		//Ex: 03.000
        Scanner clavier = new Scanner(System.in).useLocale(Locale.US);
        /*ceci force les paramètres locaux à USA. donc au lieu
         * de virgule, on force à utiliser point sur les nombres réels
         */
        String pattern = "[0-9]{2}[.]{1}[0-9]{1,3}";
        boolean nombreValide = false;
        Double nombreSaisie=0.0;
        while(!nombreValide){
        	System.out.println("Entrez un nombre réel ..");
        	System.out.println("2 chiffres pour la partie entière");
        	System.out.println("1 à 3 chiffres pour la partie décimale");
        	if(clavier.hasNext(pattern)){
        		nombreSaisie=clavier.nextDouble();
        		nombreValide = true; //pour arreter la boucle
        	}else{
        		clavier.next();
        	}
        }
        System.out.format("Le nombre saisie est: %06.3f",nombreSaisie);
        clavier.close();
	}
}
