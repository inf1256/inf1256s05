/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-05 --validation code permanent
 */
package inf1256s05;
import java.util.*;
public class CodePermanent {

	public static void main(String[] args) {
		
       Scanner clavier = new Scanner(System.in);
       String pattern = "[A-Za-z]{4}[0-9]{8}";
       boolean stop=false;
       while(!stop){
    	   System.out.println("Entrez un code permanent valide");
    	   System.out.println("4 lettres suivies de 8 chiffres");
    	   if(clavier.hasNext(pattern)){
    		   stop = true;
    	   }else{
    		   System.out.println("Ce code n'est pas valide");
    		   clavier.next();
    	   } 
       }
       String codePermanent = clavier.next();
       System.out.println("Le code permanent est "+codePermanent);
       clavier.close();
	}

}
