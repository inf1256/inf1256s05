/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-05
 */
package inf1256s05;
import java.util.*;
public class ValidationReponse {

	public static void main(String[] args) {
		// Valider une réponse O,o,N ou n
		String pattern = "[OoNn]";//Expression régulière
        Scanner clavier = new Scanner(System.in);
        System.out.println("Entrez O ou o pour Oui / N ou n pour Non");
        while(!clavier.hasNext(pattern)){
        	System.out.println("Réponse invalide");
        	System.out.println("Entrez O ou o pour Oui / N ou n pour Non");
        	clavier.next();//on déplace la tête de lecture 
        	 //on lit sans mettre dans aucune variable var valeur invalide
        }
        String reponse = clavier.next();//valeur valide
        System.out.println("Votre réponse est: " + reponse);
        clavier.close();
	}

}
