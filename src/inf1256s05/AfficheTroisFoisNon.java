/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-05
 */
package inf1256s05;

public class AfficheTroisFoisNon {
    static final int MAX = 3;
	public static void main(String[] args) {
		final String NON = "NON!";
		for(int i = 0; i< MAX; i++){
			System.out.println(NON);
		} 
	}
}
