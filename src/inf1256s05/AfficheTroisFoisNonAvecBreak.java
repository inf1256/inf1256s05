/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-05
 */
package inf1256s05;

public class AfficheTroisFoisNonAvecBreak {
    static final int MAXI = 3;
    static final int MAXJ = 5;
	public static void main(String[] args) {
		final String NON = "NON!";
		etiquette: for(int j=0;j< MAXJ;j++){
		  System.out.format("%n Ligne : %d ",j);
		 for(int i = 0; i< MAXI; i++){
			System.out.format(" %s",NON);
			if((j == MAXI) && (i == 1)){
				break etiquette;// arrete la boucle de l'étiquette
			}
		} 
		}
	}
}
