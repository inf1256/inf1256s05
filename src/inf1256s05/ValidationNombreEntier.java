/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-05
 */
package inf1256s05;
import java.util.*;
public class ValidationNombreEntier {

	public static void main(String[] args) {
		// Validation nombre entier avec do ..while
        Scanner clavier = new Scanner(System.in);
        boolean nombreValide = false;
        int nombreSaisie=0;
        // on pourrait aussi le valider avec le pattern "[0-9]+"
        do{
        	System.out.format("Entrez un nombre entier svp %n");
        	if(clavier.hasNextInt()){
        		nombreSaisie =clavier.nextInt();
        		nombreValide = true; //pour arreter la boucle
        	}else{
            System.out.format("Pas un nombre entier valide %n");
        	clavier.next();	
        	}
        }while(!nombreValide);
        System.out.format("Le nombre saisie est %d %n", nombreSaisie);
        clavier.close();
	}

}
