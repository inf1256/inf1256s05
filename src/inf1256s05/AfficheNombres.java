/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-02-05
 */
package inf1256s05;

public class AfficheNombres {
    static final int MAX = 10;
	public static void main(String[] args) {
		int i = 1;
		while (i<= MAX){ // affiche 1 a 10
			System.out.println(" i = "+i);
			i = i + 1;
		}
      
	}

}
