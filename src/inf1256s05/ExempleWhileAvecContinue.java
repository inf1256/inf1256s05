/*
 * @ Author: Johnny Tsheke @ UQAM
 * 2017-11-27
 */
package inf1256s05;

public class ExempleWhileAvecContinue {
    static final int MAX = 3;
	public static void main(String[] args) {
		final String NON = "NON!";
		int i=0;
		while( i< MAX){
			i++;
			if(i==1){
				continue;
			}
			System.out.println(NON);
		} 
	}
}
